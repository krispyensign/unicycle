module unicycle

using Spark

function getCandidates(
  sc::SparkContext,
  path::Core.Array,
  neighbors::JavaRDD
)::PipelinedRDD
  # prep the neighbors for processing
  nbrRdd::JavaRDD = parallelize(sc, neighbors[path[length(path) - 1]])
  
  # assert that the current neighbor isn't a loop
  # and if the neighbor completes the circuit or is not already included
  pathOrCycles::PipelinedRDD = filter(nbrRdd, path -> path[length(path) - 1] != neighbor && 
        in(path, neighbor) == (path[0] == neighbor))
  
  # construct new paths with remaining neighbors
  newPaths::PipelinedRDD = flat_map(rdd, hcat(pathOrCycles, neighbor))
  return newPaths
end

function growPaths(
  sc::SparkContext,
  paths::Union{JavaRDD, PipelinedRDD},
  neighbors::JavaRDD
)::PipelinedRDD
  # get candidates for those paths that have nbrs
  return map(
     # does this path have neighbors at the endpoint?
     filter(paths, path -> haskey(neighbors, path[path.length - 1])),

     # get candidates for the ones that do
     path -> getCandidates(sc, path, neighbors))
end

function init(neighbors::Base.Dict)::Tuple{SparkContext, JavaRDD}
  # init spark
  Spark.init()

  # connect to the server and get a context
  sc::SparkContext = SparkContext(master="local")

  nbrs = parallelize(sc, neighbors)

  # return the connection objects
  return (sc, nbrs)
end # init

function main()
  sc, nbrs = init(Dict{

  })
  close(sc)
end

main()

end # module
